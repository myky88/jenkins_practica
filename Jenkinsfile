build_result = 'NOT_EXECUTED'
continue_build = true
end_close_docker = true

JOB_CUT_NAME = JOB_NAME.split('/')[-1]
RESULTS_DIR = JENKINS_HOME + "/workspace/" + JOB_NAME + "/results"
env.MAIL_FAIL_TO = 'mykyro8@gmail.com'
env.CONFIG_FILE = "projects/${JOB_CUT_NAME}/config/config.yml"
env.DOCKER_BIN = 'sudo /usr/bin/docker'
env.DOCKER_IMAGE = 'selenium/standalone-chrome-debug:3.3.0-penguin'
env.PATH = '/usr/local/rvm/gems/ruby-2.3.1/bin:/usr/local/rvm/gems/ruby-2.3.1@global/bin:/usr/local/rvm/rubies/ruby-2.3.1/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/usr/local/rvm/bin'
env.GEM_HOME = '/usr/local/rvm/gems/ruby-2.3.1@global'
env.GEM_PATH = '/usr/local/rvm/gems/ruby-2.3.1@global'
env.MY_RUBY_HOME = '/usr/local/rvm/rubies/ruby-2.3.1'
env.IRBRC = '/usr/local/rvm/rubies/ruby-2.3.1/.irbrc'
env.RUBY_VERSION = 'ruby-2.3.1'

stage('Preparation') {
    node {
        sh "if [[ -d ${RESULTS_DIR} ]]; then rm -fr ${RESULTS_DIR}; mkdir ${RESULTS_DIR}; else mkdir ${RESULTS_DIR}; fi"
        git url: 'git@bitbucket.org:myky88/jenkins_practica.git'
        sh """
            gem install bundle
            bundle install
        """
    }
}

stage('Start Docker') {
    node {
        try {
            sh "$DOCKER_BIN stop $JOB_CUT_NAME; $DOCKER_BIN rm $JOB_CUT_NAME;"
        } catch (err) {
            echo "Docker prepare to start"
        }
        sh """
            $DOCKER_BIN run -d --name $JOB_CUT_NAME -v /dev/shm:/dev/shm -p $DOCKER_PORT:4444 $DOCKER_IMAGE
        """
    }
}

stage('Execute Test') {
    node {
        build_result = 'FAILED'
        sh """
            cucumber -msx --format pretty --format json --out ${RESULTS_DIR}/${JOB_CUT_NAME}.json
        """
        build_result = 'SUCCESS'
    }
}

stage('Generate reports') {
    node {
        step([$class: 'CucumberReportPublisher',
            fileExcludePattern: '',
            fileIncludePattern: "${JOB_CUT_NAME}.json",
            ignoreFailedTests: false,
            jenkinsBasePath: '',
            jsonReportDirectory: "${RESULTS_DIR}/",
            missingFails: false,
            parallelTesting: false,
            pendingFails: false,
            skippedFails: false,
            undefinedFails: false])
    }
}

stage('Close Docker') {
    node {
        try {
            sh "$DOCKER_BIN stop $JOB_CUT_NAME; $DOCKER_BIN rm $JOB_CUT_NAME;"
        } catch (err) {
            echo "Docker closed"
        }
    }
}

stage('Notification') {
    node {
        try {
            if (build_result == 'SUCCESS') {
                currentBuild.result = 'SUCCESS'
                notification('GREEN')
            } else if (build_result == 'FAILED') {
                currentBuild.result = 'FAILURE'
                notification('YELLOW')
            } else {
                throw new Exception('Failed')
            }
        } catch (err) {
            build_result = 'ERROR'
            currentBuild.result = 'FAILURE'
            err_notification('RED')
        }
    }
}

def current_time(){
    return new Date().format('dd/MM/yyyy HH:mm:ss')
}

def notification (String color) {
    emailext (to: "${MAIL_SUCCESS_TO}",
        subject: "${build_result}: ${JOB_CUT_NAME} (${env.BUILD_NUMBER}) ${current_time()}.",
        body: "${message()}",
        attachmentsPattern: "results/${JOB_CUT_NAME}.json",
        mimeType:'text/html')
    hipchat_msg (color)
}

def message() {
    if (build_result == 'SUCCESS') {
        return "PASSED: All scenarios passed correctly and have QA PASS. Sprint can deploy to production."
    } else {
        return "FAILED: There are failed scenarios. Errors listed in JSON file."
    }
}

def err_notification (String color) {
    step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: "${MAIL_FAIL_TO}", sendToIndividuals: true])
    hipchat_msg (color)
}

def hipchat_msg (String color) {
    hipchatSend (color: "${color}", notify: true,
        message: "<strong>${JOB_CUT_NAME} [${env.BUILD_NUMBER}]</strong>: tests was <strong>${build_result}</strong>. Url: <a href=\"${env.BUILD_URL}\">${env.BUILD_URL}</a>"
    )
}