Feature: Checking Sign In Google page
  As a Visitor
  I want to run all possible actions in Sign In Google page
  So all actions work as expected

  Scenario: Open Sign In Google page
    Given I open my browser
    When I open the page by the url "https://accounts.google.com"
    Then The page has a input "Email"
    And The page has a button "next"

  Scenario: Go to fill first part Sign in
    Given I am in the "Google Sign In" page
    When I fill the input "Email" with "DB=valid_user_mail"
    And I click the button "next"
    Then The page has a input "Passwd"
    And The page has a button "signIn"

  Scenario: Go to fill second part Sign in
    Given I am in the "Google Sign In" page
    When I fill the input "Email" with "DB=valid_user_mail"
    And I click the button "next"
    And I fill the input "Passwd" with "DB=valid_user_pass"
    And I click the button "signIn"
    Then The page has text "Mi cuenta"

  Scenario: Go to Create account
    Given I am in the "Google Sign In" page
    When I click on link "Crear cuenta"
    Then The page has text "Crea tu cuenta de Google"

  Scenario: Go to Forgot Password
    Given I am in the "Google Sign In" page
    When I fill the input "Email" with "DB=valid_user_mail"
    And I click the button "next"
    And I click on link "¿Has olvidado la contraseña?"
    Then The page has text "Escribe la última contraseña que recuerdes."

  Scenario Outline: Get validations email errors Google Sign in
    Given I am in the "Google Sign In" page
    When I fill the input "Email" with <email>
    And I click the button "next"
    Then The page has text "La contraseña es incorrecta. Inténtalo de nuevo."

    Examples: Wrong user list
    | email             |
    | "novalid@@nyecrrf.es" |
    | "novalid@nyecrrf"     |
    | "novalid@nyecrrf.e"   |
    | "@nyecrrf.es"         |
    | "@.com"           |
    | "noemail"         |
    | ""                |

  Scenario Outline: Get validations password errors Google Sign in
    Given I am in the "Google Sign In" page
    When I fill the input "Email" with <email>
    And I click the button "next"
    And I fill the input "Passwd" with <password>
    And I click the button "signIn"
    Then The page has text "La contraseña es incorrecta. Inténtalo de nuevo."

  Examples: Wrong password list
      | email                     | password         |
      | "DB=valid_user_mail"      | "12345678"       |
      | "DB=valid_user_mail"      | "Wrong-password" |
      | "DB=valid_user_mail"      | "34%!·$%·$"     |
