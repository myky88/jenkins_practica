Given(/^I open my browser$/) do
  sleep [2,3,5].sample
end

Given(/^I am in the "([^"]*)" page$/) do |arg1|
  sleep [2,3,5].sample
end

When(/^I open the page by the url "([^"]*)"$/) do |arg1|
  sleep [2,3,5].sample
end

When(/^I fill the input "([^"]*)" with "([^"]*)"$/) do |arg1, arg2|
  sleep [2,3,5].sample
end

When(/^I click the button "([^"]*)"$/) do |arg1|
  sleep [2,3,5].sample
end

When(/^I click on link "([^"]*)"$/) do |arg1|
  sleep [2,3,5].sample
end

Then(/^The page has a input "([^"]*)"$/) do |arg1|
  sleep [2,3,5].sample
end

Then(/^The page has a button "([^"]*)"$/) do |arg1|
  sleep [2,3,5].sample
end

Then(/^The page has text "([^"]*)"$/) do |arg1|
  sleep [2,3,5].sample
end

